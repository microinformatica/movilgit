package com.example.ejemplogit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView  txtid = (TextView) findViewById((R.id.txtID));
         Button  btn      = (Button) findViewById(R.id.btnPulsar);


         btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 txtid.setText("hola una vez mas");
             }
         });
    }


}
